.section .text
	.global strlen

strlen:
	push ebp
	mov ebp, esp
	
	mov eax,[ebp+8]
	dec eax
	
	__strlen__loop_1:
	inc eax
	mov bl,[eax]
	test bl,bl
	jnz __strlen__loop_1
	
	sub eax,[ebp+8]

	mov esp,ebp
        pop ebp

	ret

	
