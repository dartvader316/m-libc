.global print_char
.global scan_char
.global print_int32
.global scan_int32
write:
	mov eax,4
	int 0x80
	ret
read:
	mov eax,3
	int 0x80
	ret
scan_int32:
	push ebp
	mov ebp,esp
	
	sub esp,12+4
	
	xor eax,eax
	mov [ebp-4],eax
	mov [ebp-8],eax
	mov [ebp-12],eax
	
	mov ebx,0
	lea ecx,[ebp-12]
	mov edx,12
	call read
	
	xor al,al
	mov al,[ebp-1]
	test al,al
	jnz __si__error
	
	xor eax,eax
	mov [ebp-16],eax
	
	mov eax,1
	xor ebx,ebx

	__si__loop_1:
	
	add ebx,ebp
	push ebx
	
	mov edx,ebx
	xor ebx,ebx
	
	mov bl, [edx]
	cmp bl,'-'
	je __si__neg_end
	cmp bl,48
	jl __si__not_int
	cmp bl,48+10
	jg __si__not_int
	sub bl,48
	
	mov ecx,eax
	xor edx,edx
	mul ebx
	
	mov edx,[ebp-16]
	add edx,eax
	mov [ebp-16],edx
	
	mov eax,ecx
	mov ecx,10
	mul ecx
	
	__si__not_int:
	
	pop ebx
	dec ebx

	sub ebx,ebp
	
	cmp ebx,-12-1
	jne __si__loop_1
	jmp __si__pos_end
	
	__si__neg_end:
	mov eax,[ebp-16]
	neg eax
	jmp __si__ret
	
	__si__pos_end:
	mov eax,[ebp-16]
	jmp __si__ret

	__si__error:
	mov eax,-1
	__si__ret:
	mov esp,ebp
        pop ebp

	ret
	
print_int32:
	push ebp
	mov ebp, esp

	sub esp,4
	
	mov eax,[ebp+8]
	test eax,eax
	jnl __pi__pos
	
	neg eax
	mov [ebp+8],eax

	mov eax,'-'
	mov [ebp-4],eax
	lea ecx,[ebp-4]
	mov edx,1
	mov ebx,1
	call write

	__pi__pos:
	
	mov eax,0
	mov [ebp-4],eax

	__pi__loop_1:
	mov eax,[ebp-4]
	inc eax
	mov [ebp-4],eax

	mov eax,[ebp+8]
	mov edx,0
	mov ecx,10
	div ecx
	
	add dl,48
	sub esp,1
	mov [esp],dl
	mov [ebp+8],eax

	test eax,eax
	jnz __pi__loop_1

	mov ebx,1
	mov ecx,esp
	mov edx,[ebp-4]
	
	call write
	
	mov esp,ebp
        pop ebp

	ret

print_char:
	
	push ebp
	mov ebp, esp
	
	mov ebx,1
	lea ecx,[ebp+8]
	mov edx,1
	call write

	mov esp,ebp
        pop ebp

	ret

scan_char:
	push ebp
	mov ebp, esp

	sub esp,4

	mov ebx,0
	lea ecx,[ebp-4]
	mov edx,1
	call read
	mov eax,[ebp-4]

	mov esp,ebp
        pop ebp
	
	ret
