#ifndef __M_C_LIB_EXPERIMENTAL
#define __M_C_LIB_EXPERIMENTAL

#warning Compiling with experimental functions
extern void* malloc(unsigned long n);
extern void free(void* p);

#endif
