#ifndef __M_C_LIB_IO
#define __M_C_LIB_IO

extern void print_char(char a);
extern int scan_char();
#define putch print_char
#define getch scan_char

extern void print_int32(int a);
extern int scan_int32();
#define puti print_int32
#define geti scan_int32

#endif
