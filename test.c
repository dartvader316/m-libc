#include "io.h"
#include "string.h"

void gets(char* a,int len){
	int i =0;
	char buff;
	for(;i<len-1;i++){
		buff = scan_char();
		if(buff==10) break;
		else a[i] = buff;
	}
	a[i] = 0;
}

void puts(char* a){
	for(;(*a)!=0;a++){
		print_char(*a);
	}
	putch(10);
}

#if defined __expv__

#endif

void inputTest(){
	char buffer[64];

	gets(buffer,64);
	puts(buffer);
	puts("Length:");
	puti(strlen(buffer));
	putch(10);
}
void intTest(){
	puti(-10);
	putch(10);
	puti(0x7FFFFFFF);
	putch(10);
	puti(-0x7FFFFFFF);
	putch(10);

}
void intInputTest(){
	int a;
	while(a!=316){
		a = geti();
		puti(a);
		putch(10);
	}
}
int main(){
	intInputTest();
	inputTest();
	intTest();
	return 1;
}

