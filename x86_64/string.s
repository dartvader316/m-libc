.global strlen

strlen:
	push rbp
	mov rbp, rsp
	
	mov rax,rdi
	dec rax
	
	__strlen__loop_1:
	inc rax
	mov bl,[rax]
	test bl,bl
	jnz __strlen__loop_1
	
	sub rax,rdi

	mov rsp,rbp
        pop rbp

	ret

	
