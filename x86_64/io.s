.global print_char
.global scan_char
.global print_int32
.global scan_int32
write:
	mov rax,1
	syscall
	ret
read:
	mov rax,0
	syscall
	ret

scan_int32:
	push rbp
	mov rbp,rsp
	
	sub rsp,12+4
	
	xor eax,eax
	mov [rbp-4],eax
	mov [rbp-8],eax
	mov [rbp-12],eax
	
	mov rdi,0 
	lea rsi,[rbp-12]
	mov rdx,12
	call read
	
	xor al,al
	mov al,[rbp-1]
	test al,al
	jnz __si__error
	
	xor eax,eax
	mov [rbp-16],eax
	
	mov eax,1
	xor ebx,ebx

	__si__loop_1:
	
	add rbx,rbp
	push rbx
	
	mov rdx,rbx
	xor ebx,ebx
	
	mov bl, [rdx]
	cmp bl,'-'
	je __si__neg_end
	cmp bl,48
	jl __si__not_int
	cmp bl,48+10
	jg __si__not_int
	sub bl,48
	
	mov ecx,eax
	xor edx,edx
	mul ebx
	
	mov edx,[rbp-16]
	add edx,eax
	mov [rbp-16],edx
	
	mov eax,ecx
	mov ecx,10
	mul ecx
	
	__si__not_int:
	
	pop rbx
	dec rbx

	sub rbx,rbp
	
	cmp rbx,-12-1
	jne __si__loop_1
	jmp __si__pos_end
	
	__si__neg_end:
	mov eax,[rbp-16]
	neg eax
	jmp __si__ret
	
	__si__pos_end:
	mov eax,[rbp-16]
	jmp __si__ret

	__si__error:
	mov rax,-1
	__si__ret:
	mov rsp,rbp
        pop rbp

	ret
	
print_int32:
	push rbp
	mov rbp, rsp

	sub rsp,8+8

	mov eax,edi
	mov [rbp-16],eax
	test eax,eax
	jnl __pi__pos
	
	neg eax
	mov [rbp-16],eax
	
	mov al,'-'
	mov [rbp-8],al
	lea rsi,[rbp-8]
	mov rdi,1
	mov rdx,1
	call write

	__pi__pos:
	
	mov rax,0
	mov [rbp-8],rax

	__pi__loop_1:
	mov rax,[rbp-8]
	inc rax
	mov [rbp-8],rax

	mov eax,[rbp-16]
	mov edx,0
	mov ecx,10
	div ecx
	
	add dl,48
	sub rsp,1
	mov [rsp],dl
	mov [rbp-16],eax

	test eax,eax
	jnz __pi__loop_1

	mov rdi,1
	mov rsi,rsp
	mov rdx,[rbp-8]
	
	call write
	
	mov rsp,rbp
        pop rbp

	ret

print_char:
	pushq rbp
	mov rbp, rsp

	sub rsp,8
	mov [rbp-8],rdi
	
	mov rdi,1 
	lea rsi,[rbp-8]
	mov rdx,1
	call write

	mov rsp,rbp
        popq rbp
	ret

scan_char:
	pushq rbp
	mov rbp, rsp

	sub rsp,4

	mov rdi,0 
	lea rsi,[rbp-4]
	mov rdx,1
	call read

	mov rax,[rbp-4]

	mov rsp,rbp
        popq rbp
	
	ret

