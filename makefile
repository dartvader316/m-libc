OUTFILENAME = test
NODEFAULTLIBS = -ffreestanding -fno-stack-protector -nostdlib -nodefaultlibs 

x86: clear
	gcc -m32 $(NODEFAULTLIBS) -Wall -g test.c lib.S -o $(OUTFILENAME)
x86_64:	clear
	gcc $(NODEFAULTLIBS) -Wall -g test.c lib.S -o $(OUTFILENAME)

x86-exp: clear
	gcc -D __expv__ -m32 $(NODEFAULTLIBS) -Wall -g test.c lib.S -o $(OUTFILENAME)
x86_64-exp: clear
	gcc -D __expv__ -no-pie $(NODEFAULTLIBS) -Wall -g test.c lib.S -o $(OUTFILENAME)
clear:
	if [ -e $(OUTFILENAME) ]; then rm -Rf $(OUTFILENAME); fi
